package ru.tsc.almukhametov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.model.Task;

import java.util.List;
import java.util.Optional;

public interface IProjectTaskService {

    @NotNull
    List<Task> findAllTaskByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task bindTaskById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    Task unbindTaskById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    Optional<Project> removeById(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Optional<Project> removeByIndex(@Nullable String userId, Integer index);

    @NotNull
    Optional<Project> removeByName(@Nullable String userId, @Nullable String name);

    void clearProjects(@Nullable String userId);

}
