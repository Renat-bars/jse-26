package ru.tsc.almukhametov.tm.exception.empty;

import ru.tsc.almukhametov.tm.exception.AbstractException;

public class EmptyFirstNameException extends AbstractException {

    public EmptyFirstNameException() {
        super("Error!First name is empty");
    }

}
