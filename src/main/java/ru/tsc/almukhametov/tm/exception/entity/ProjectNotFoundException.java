package ru.tsc.almukhametov.tm.exception.entity;

import ru.tsc.almukhametov.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error. Project not found.");
    }

}
