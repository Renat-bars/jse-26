package ru.tsc.almukhametov.tm.constant;

import org.jetbrains.annotations.NotNull;

public class SystemDescriptionConst {

    @NotNull
    public static final String ABOUT = "display developer info.";
    @NotNull
    public static final String HELP = "display list of commands";
    @NotNull
    public static final String VERSION = "display program version.";
    @NotNull
    public static final String INFO = "display system information.";
    @NotNull
    public static final String COMMANDS = "display list of commands.";
    @NotNull
    public static final String ARGUMENTS = "display list arguments.";
    @NotNull
    public static final String PROJECT_LIST = "show project list.";
    @NotNull
    public static final String PROJECT_CREATE = "create new project.";
    @NotNull
    public static final String PROJECT_CLEAR = "remove all project.";
    @NotNull
    public static final String PROJECT_SHOW_BY_ID = "show project by id.";
    @NotNull
    public static final String PROJECT_SHOW_BY_INDEX = "show project by index.";
    @NotNull
    public static final String PROJECT_SHOW_BY_NAME = "show project by name.";
    @NotNull
    public static final String PROJECT_REMOVE_BY_ID = "remove project by id.";
    @NotNull
    public static final String PROJECT_REMOVE_BY_INDEX = "remove project by index.";
    @NotNull
    public static final String PROJECT_REMOVE_BY_NAME = "remove project by name.";
    @NotNull
    public static final String PROJECT_UPDATE_BY_ID = "update project by id.";
    @NotNull
    public static final String PROJECT_UPDATE_BY_INDEX = "update project by index.";
    @NotNull
    public static final String PROJECT_START_BY_ID = "start project by id.";
    @NotNull
    public static final String PROJECT_START_BY_INDEX = "start project by index.";
    @NotNull
    public static final String PROJECT_START_BY_NAME = "start project by name.";
    @NotNull
    public static final String PROJECT_FINISH_BY_ID = "finish project by id.";
    @NotNull
    public static final String PROJECT_FINISH_BY_INDEX = "finish project by index.";
    @NotNull
    public static final String PROJECT_FINISH_BY_NAME = "finish project by name.";
    @NotNull
    public static final String PROJECT_CHANGE_STATUS_BY_ID = "change project status  by id.";
    @NotNull
    public static final String PROJECT_CHANGE_STATUS_BY_INDEX = "change project status by index.";
    @NotNull
    public static final String PROJECT_CHANGE_STATUS_BY_NAME = "change project status by name.";
    @NotNull
    public static final String TASK_LIST = "show task list.";
    @NotNull
    public static final String TASK_CREATE = "create new task.";
    @NotNull
    public static final String TASK_CLEAR = "remove all task.";
    @NotNull
    public static final String TASK_SHOW_BY_ID = "show task by id.";
    @NotNull
    public static final String TASK_SHOW_BY_INDEX = "show task by index.";
    @NotNull
    public static final String TASK_SHOW_BY_NAME = "show task by name.";
    @NotNull
    public static final String TASK_REMOVE_BY_ID = "remove task by id.";
    @NotNull
    public static final String TASK_REMOVE_BY_INDEX = "remove task by index.";
    @NotNull
    public static final String TASK_REMOVE_BY_NAME = "remove task by name.";
    @NotNull
    public static final String TASK_UPDATE_BY_ID = "update task by id.";
    @NotNull
    public static final String TASK_UPDATE_BY_INDEX = "update task by index.";
    @NotNull
    public static final String TASK_START_BY_ID = "start task by id.";
    @NotNull
    public static final String TASK_START_BY_INDEX = "start task by index.";
    @NotNull
    public static final String TASK_START_BY_NAME = "start task by name.";
    @NotNull
    public static final String TASK_FINISH_BY_ID = "finish task by id.";
    @NotNull
    public static final String TASK_FINISH_BY_INDEX = "finish task by index.";
    @NotNull
    public static final String TASK_FINISH_BY_NAME = "finish task by name";
    @NotNull
    public static final String TASK_CHANGE_STATUS_BY_ID = "change task status  by id.";
    @NotNull
    public static final String TASK_CHANGE_STATUS_BY_INDEX = "change task status by index.";
    @NotNull
    public static final String TASK_CHANGE_STATUS_BY_NAME = "change task status by name.";
    @NotNull
    public static final String TASK_LIST_BY_PROJECT = "show task list by project.";
    @NotNull
    public static final String TASK_BIND_TO_PROJECT = "bind task to project.";
    @NotNull
    public static final String TASK_UNBIND_FROM_PROJECT = "unbind task from project.";
    @NotNull
    public static final String USER_LIST = "show user list";
    @NotNull
    public static final String USER_CREATE = "create new user";
    @NotNull
    public static final String USER_CLEAR = "clear user";
    @NotNull
    public static final String USER_SHOW_BY_ID = "show user by id";
    @NotNull
    public static final String USER_SHOW_BY_LOGIN = "show user by login";
    @NotNull
    public static final String USER_REMOVE_BY_ID = "remove user by id";
    @NotNull
    public static final String USER_REMOVE_BY_LOGIN = "remove user by login";
    @NotNull
    public static final String USER_UPDATE_BY_ID = "update user by id";
    @NotNull
    public static final String USER_UPDATE_BY_LOGIN = "update user by login";
    @NotNull
    public static final String USER_CHANGE_PASSWORD = "change user password";
    @NotNull
    public static final String USER_CHANGE_ROLE = "change user role";
    @NotNull
    public static final String USER_LOCK_BY_LOGIN = "user lock by login";
    @NotNull
    public static final String USER_UNLOCK_BY_LOGIN = "user unlock by login";
    @NotNull
    public static final String LOGIN = "login";
    @NotNull
    public static final String LOGOUT = "logout";
    @NotNull
    public static final String EXIT = "close application.";

}
