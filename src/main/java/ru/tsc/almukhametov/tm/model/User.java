package ru.tsc.almukhametov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.enumerated.Role;

@Getter
@Setter
public class User extends AbstractEntity {

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @NotNull
    private Role role = Role.USER;

    private boolean locked = false;

    public User() {
    }

}
