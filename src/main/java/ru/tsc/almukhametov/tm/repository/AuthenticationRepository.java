package ru.tsc.almukhametov.tm.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.repository.IAuthenticationRepository;

public class AuthenticationRepository implements IAuthenticationRepository {

    private String currentUserId;

    @Nullable
    @Override
    public String getCurrentUserId() {
        return currentUserId;
    }

    @Override
    public void setCurrentUserId(@Nullable String currentUserId) {
        this.currentUserId = currentUserId;
    }
}
