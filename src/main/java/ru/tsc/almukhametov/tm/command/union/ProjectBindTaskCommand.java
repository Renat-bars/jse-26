package ru.tsc.almukhametov.tm.command.union;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.command.AbstractProjectTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public final class ProjectBindTaskCommand extends AbstractProjectTaskCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.TASK_BIND_TO_PROJECT;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.TASK_BIND_TO_PROJECT;
    }

    @Override
    public void execute() {
        @NotNull final String userId = String.valueOf(serviceLocator.getAuthenticationService().getCurrentUserId());
        System.out.println("[Bind task to project]");
        System.out.println("Enter task id");
        @Nullable final String taskId = TerminalUtil.nextLine();
        if (taskId == null) throw new TaskNotFoundException();
        System.out.println("Enter project Id");
        @Nullable final String projectId = TerminalUtil.nextLine();
        if (projectId == null) throw new ProjectNotFoundException();
        @Nullable final Task task = serviceLocator.getProjectTaskService().bindTaskById(userId, projectId, taskId);
        if (task == null) throw new TaskNotFoundException();
    }

}
