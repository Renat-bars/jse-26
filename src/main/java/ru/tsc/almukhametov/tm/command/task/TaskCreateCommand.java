package ru.tsc.almukhametov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.command.AbstractTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.TASK_CREATE;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.TASK_CREATE;
    }

    @Override
    public void execute() {
        @NotNull final String userId = String.valueOf(serviceLocator.getAuthenticationService().getCurrentUserId());
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().create(userId, name, description);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
