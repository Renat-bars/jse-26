package ru.tsc.almukhametov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.service.IServiceLocator;
import ru.tsc.almukhametov.tm.enumerated.Role;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public Role[] roles() {
        return null;
    }

    @NotNull
    public abstract String name();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    @NotNull
    public abstract void execute();

    @Nullable
    @Override
    public String toString() {
        @Nullable String result = "";
        @Nullable String name = name();
        @Nullable String arg = arg();
        @Nullable String description = description();

        if (name != null && !name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += " [" + arg + "] ";
        if (description != null && !description.isEmpty()) result += " --- " + description;
        return result;
    }

}
