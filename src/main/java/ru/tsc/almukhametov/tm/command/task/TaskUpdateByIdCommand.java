package ru.tsc.almukhametov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.command.AbstractTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.exception.empty.EmptyIdException;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.TASK_UPDATE_BY_ID;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.TASK_UPDATE_BY_ID;
    }

    @Override
    public void execute() {
        @NotNull final String userId = String.valueOf(serviceLocator.getAuthenticationService().getCurrentUserId());
        System.out.println("Enter Id");
        @NotNull final String id = TerminalUtil.nextLine();
        if (!serviceLocator.getTaskService().existById(userId, id)) throw new EmptyIdException();
        System.out.println("Enter Name");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final Task taskUpdated = serviceLocator.getTaskService().updateById(userId, id, name, description);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
