package ru.tsc.almukhametov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.command.AbstractProjectCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeByNameCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.PROJECT_CHANGE_STATUS_BY_NAME;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.PROJECT_CHANGE_STATUS_BY_NAME;
    }

    @Override
    public void execute() {
        @Nullable final String userId = String.valueOf(serviceLocator.getAuthenticationService().getCurrentUserId());
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("Enter Name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter Status");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.valueOf(statusValue);
        @Nullable final Project project = serviceLocator.getProjectService().changeProjectStatusByName(userId, name, status);
        if (project == null) throw new ProjectNotFoundException();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
