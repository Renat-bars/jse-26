package ru.tsc.almukhametov.tm;

import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static ru.tsc.almukhametov.tm.util.UnitUtil.convertBytes;

/**
 * Unit test for simple App.
 */
public class ApplicationTest {
    public static void main(final String[] args) {
        final long[] l = new long[]{1l, 4343l, 43434334l, 3563543743l};
        for (final long ll : l) {
            System.out.println(convertBytes(ll));
        }
    }

    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }
}

